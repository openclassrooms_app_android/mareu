# MaReu

`MaReu` est une application qui permet de trouver une salle de réunion disponible très rapidement !

## Installation
Pour récupérer l'application, vous pouvez :
- Cloner l'application via cette commande `git clone https://gitlab.com/openclassrooms_app_android/mareu`
- Telécharger l'archive ci-dessus via le bouton de teléchargement

## Utilisation
Pour utiliser cette application, nous vous conseillons d'utiliser un IDE comme [Android Studio](https://developer.android.com/studio) afin de pouvoir compiler celle-ci sur un emulateur ou/et sur votre smartphone.

### Dépendances utilisées

```groovy
dependencies {
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'com.google.android.material:material:1.2.1'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'androidx.navigation:navigation-fragment:2.3.2'
    implementation 'androidx.navigation:navigation-ui:2.3.2'
    implementation 'androidx.annotation:annotation:1.1.0'
    implementation 'androidx.lifecycle:lifecycle-livedata-ktx:2.2.0'
    implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.core:core:1.5.0-beta01'

    testImplementation 'junit:junit:4.13.1'
    testImplementation 'androidx.test:core:1.3.0'

    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test:rules:1.3.0'
    androidTestImplementation 'androidx.test:runner:1.3.0'

    androidTestImplementation 'androidx.test.espresso:espresso-core:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-contrib:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-intents:3.1.0'
}
```

`ATTENTION: Les dépendances ci-dessous ne doivent pas être mises à jour inutilement cela empêcherait le bon fonctionnement des tests !`
```groovy
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-contrib:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-intents:3.1.0'
```

## Auteur
[Valentin Etudiant OpenClassrooms](https://openclassrooms.com/fr/members/5zbcwst2hpq8)

## Licence

```text
Copyright 2020 MaReu Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this application and associated documentation files (the "MaReu Application"), to deal in the Application without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Application, and to permit persons to whom the Application is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Application.

THE APPLICATION IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE APPLICATION OR THE USE OR OTHER DEALINGS IN THE APPLICATION.
```
