package com.valentin.mareu;

import com.valentin.mareu.di.DI;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Participant;
import com.valentin.mareu.models.Room;
import com.valentin.mareu.service.MeetingApiService;

import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import androidx.annotation.StringRes;

public class MeetingUnitTest {

    MeetingApiService service;
    private List<Object> meetings;
    private int nbOfMeetings;

    private Calendar currentDate;
    private Calendar startDate;
    private Calendar endDate;

    private SimpleDateFormat dateFR;

    private List<Participant> participants;
    private Meeting meetingTest;

    @Before
    public void setUp() throws Exception {
        service = DI.getMeetingApiService();
        meetings = service.getMeetings();
        nbOfMeetings = 10;

        // DATE
        currentDate = Calendar.getInstance(Locale.FRANCE);
        startDate = Calendar.getInstance(Locale.FRANCE);
        endDate = Calendar.getInstance(Locale.FRANCE);
        endDate.set(Calendar.HOUR_OF_DAY, startDate.get(Calendar.HOUR_OF_DAY) + 1);

        // FORMATTER DATE
        dateFR = new SimpleDateFormat("dd/MM/y");

        // CREATE NEW MEETING TEST
        participants = new ArrayList<>();
        participants.add(new Participant("test@gmail.com"));
        participants.add(new Participant("test2@gmail.com"));
        meetingTest = new Meeting(meetings.size() + 1, "Sujet test", startDate, endDate, new Room(service.getAllMeetingRooms().size(), R.string.salle_a, R.color.color_red_800), participants);
    }

    /**
     * Vérifie l'ajout et la suppression de réunion
     */
    @Test
    public void test_checkMeetingAdditionAndDeletion() {
        assertEquals(nbOfMeetings, service.getMeetings().size());

        // Add
        service.addMeeting(meetingTest);
        assertEquals(nbOfMeetings + 1, meetings.size());
        assertTrue(meetings.contains(meetingTest));

        // Delete
        service.deleteMeeting(meetingTest);
        assertEquals(nbOfMeetings, service.getMeetings().size());
        assertFalse(meetings.contains(meetingTest));
    }

    /**
     * Vérifie que la liste est bien filtré par date
     */
    @Test
    public void test_checkIfDataHasBeenFilterByDate() {
        String currentDateText = dateFR.format(currentDate.getTime());

        List<Object> dataFilterByDate = service.getMeetingsFilterByDate(currentDate);
        assertEquals(1, dataFilterByDate.size());

        Meeting meetingTest = (Meeting) dataFilterByDate.get(0);
        String meetingDateText = dateFR.format(meetingTest.getStartDate().getTime());

        assertEquals(currentDateText, meetingDateText);
    }

    /**
     * Vérifie que la liste est bien filtré par salle
     */
    @Test
    public void test_checkIfDataHasBeenFilterByPlace() {
        @StringRes int roomTest = R.string.salle_a;

        List<Object> dataFilterByPlace = service.getMeetingsFilterByPlace(roomTest);
        assertEquals(1, dataFilterByPlace.size());
        Meeting data = (Meeting) dataFilterByPlace.get(0);

        assertEquals(data.getRoom().getName(), roomTest);
    }
}
