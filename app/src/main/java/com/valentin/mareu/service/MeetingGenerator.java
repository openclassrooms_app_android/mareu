package com.valentin.mareu.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.Log;

import com.valentin.mareu.R;
import com.valentin.mareu.di.DI;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Participant;
import com.valentin.mareu.models.Room;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import androidx.annotation.ColorRes;
import androidx.annotation.StringRes;

public abstract class MeetingGenerator {

    private static final String LOG = MeetingGenerator.class.getSimpleName();

    /**
     * Generate Rooms object
     * @return
     */
    public static List<Room> generateMeetingRooms() {
        List<Room> meetingRooms = new ArrayList<>();

        int[] getResourceMeetingRooms = new int[] {R.string.salle_a, R.string.salle_b, R.string.salle_c,
                R.string.salle_d, R.string.salle_e, R.string.salle_f,
                R.string.salle_g, R.string.salle_h, R.string.salle_i, R.string.salle_j};

        int[] getColors = new int[] {R.color.color_red_800, R.color.color_red_600, R.color.color_red_500,
                R.color.color_green_800, R.color.color_green_600, R.color.color_green_500,
                R.color.color_blue_800, R.color.color_blue_600, R.color.color_blue_500, R.color.color_purple};

        for(int i = 0; i < getResourceMeetingRooms.length; i++) {

            @StringRes int nameRoom = getResourceMeetingRooms[i];
            @ColorRes int colorRoom = getColors[i];

            meetingRooms.add(new Room(i, nameRoom, colorRoom));

        }

        return meetingRooms;
    }

    public static List<Object> generateMeetings() {

        Calendar currentDate = Calendar.getInstance(Locale.FRANCE);
        currentDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
        currentDate.set(Calendar.HOUR_OF_DAY, currentDate.get(Calendar.HOUR_OF_DAY) + 1);

        Calendar currentDate2 = Calendar.getInstance(Locale.FRANCE);
        currentDate2.set(Calendar.MONTH, currentDate2.get(Calendar.MONTH));
        currentDate2.set(Calendar.HOUR_OF_DAY, currentDate2.get(Calendar.HOUR_OF_DAY) + 2);

        Calendar dateTest = Calendar.getInstance(Locale.FRANCE);
        dateTest.set(Calendar.DAY_OF_MONTH, 25);
        dateTest.set(Calendar.MONTH, 11);
        dateTest.set(Calendar.YEAR, dateTest.get(Calendar.YEAR));
        dateTest.set(Calendar.HOUR_OF_DAY, 12);
        dateTest.set(Calendar.MINUTE, 0);

        Calendar dateTest2 = Calendar.getInstance(Locale.FRANCE);
        dateTest2.set(Calendar.DAY_OF_MONTH, 25);
        dateTest2.set(Calendar.MONTH, 11);
        dateTest2.set(Calendar.YEAR, dateTest2.get(Calendar.YEAR));
        dateTest2.set(Calendar.HOUR_OF_DAY, 13);
        dateTest2.set(Calendar.MINUTE, 0);

        List<Participant> participants = new ArrayList<>();
        participants.add(new Participant("test@gmail.com"));
        participants.add(new Participant("test3@gmail.com"));

        List<Object> MEETINGS = Arrays.asList(
                new Meeting(1, "Sujet1", currentDate, currentDate2, generateMeetingRooms().get(0), participants),
                new Meeting(2, "Sujet2", dateTest, dateTest2, generateMeetingRooms().get(1), participants),
                new Meeting(3, "Sujet3", dateTest, dateTest2, generateMeetingRooms().get(2), participants),
                new Meeting(4, "Sujet4", dateTest, dateTest2, generateMeetingRooms().get(3), participants),
                new Meeting(5, "Sujet5", dateTest, dateTest2, generateMeetingRooms().get(4), participants),
                new Meeting(6, "Sujet6", dateTest, dateTest2, generateMeetingRooms().get(5), participants),
                new Meeting(7, "Sujet7", dateTest, dateTest2, generateMeetingRooms().get(6), participants),
                new Meeting(8, "Sujet8", dateTest, dateTest2, generateMeetingRooms().get(7), participants),
                new Meeting(9, "Sujet9", dateTest, dateTest2, generateMeetingRooms().get(8), participants),
                new Meeting(10, "Sujet10", dateTest, dateTest2, generateMeetingRooms().get(9), participants)
        );
        return new ArrayList<>(MEETINGS);
    }

}
