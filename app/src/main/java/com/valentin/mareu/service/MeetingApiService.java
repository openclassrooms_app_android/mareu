package com.valentin.mareu.service;

import com.valentin.mareu.enums.FilterName;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Participant;
import com.valentin.mareu.models.Room;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.StringRes;

public interface MeetingApiService {

    /**
     * Return all meeting rooms
     * @return
     */
    List<Room> getAllMeetingRooms();

    /**
     * Return available meeting rooms
     * @return
     */
    List<Room> getMeetingRoomsAvailable(Calendar startDate, Calendar endDate);

    /**
     * Get meetings with filter by date
     * @param date
     * @return
     */
    List<Object> getMeetingsFilterByDate(Calendar date);

    /**
     * Get meetings with filter by name
     * @param name
     * @return
     */
    List<Object> getMeetingsFilterByPlace(int name);

    /**
     * Return all meeting
     * @return
     */
    List<Object> getMeetings();

    /**
     * Add new meeting
     */
    void addMeeting(Meeting meeting);

    /**
     * Delete meeting
     */
    void deleteMeeting(Meeting meeting);

    /**
     * Add participant to meeting
     * @param meeting
     * @param emailParticipants
     */
    void addParticipant(Meeting meeting, String[] emailParticipants);
}
