package com.valentin.mareu.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.valentin.mareu.R;
import com.valentin.mareu.enums.FilterName;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Participant;
import com.valentin.mareu.models.Room;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.ColorRes;
import androidx.annotation.StringRes;

public class DummyMeetingApiService implements MeetingApiService {

    private static final String LOG = DummyMeetingApiService.class.getSimpleName();
    private final List<Object> meetings = MeetingGenerator.generateMeetings();
//    private final List<Participant> participants = MeetingGenerator.generateParticipants();
    private final List<Room> listOfRooms = MeetingGenerator.generateMeetingRooms();

    // DATE FORMAT
    private SimpleDateFormat formatFR = new SimpleDateFormat("dd/MM/y HH:mm", Locale.FRANCE);
    private SimpleDateFormat dateFR = new SimpleDateFormat("dd/MM/y", Locale.FRANCE);
    private SimpleDateFormat hourFR = new SimpleDateFormat("HH:mm", Locale.FRANCE);

    /**
     * Return all meeting rooms
     * @return
     */
    public List<Room> getAllMeetingRooms() {
        return listOfRooms;
    }

    /**
     * Returns available meeting rooms
     * @return
     */
    public List<Room> getMeetingRoomsAvailable(Calendar startDateDesired, Calendar endDateDesired) {

        List<Room> meetingRooms = new ArrayList<Room>(listOfRooms);

        // Check if meeting room is available
        for(Object meeting : meetings) {

            Room roomToDelete = null;
            Calendar startDateMeeting = ((Meeting) meeting).getStartDate();
            Calendar endDateMeeting = ((Meeting) meeting).getEndDate();

//            Log.d(LOG, "[LOG DATE MEETING] " + formatFR.format(startDateMeeting.getTime()) + " --> "  + formatFR.format(endDateMeeting.getTime()));
//            Log.d(LOG, "[LOG DATE DESIRED] " + formatFR.format(startDateDesired.getTime()) + " --> "  + formatFR.format(endDateDesired.getTime()));

            // Si le créneau souhaiter se situe avant le début de la réunion et après la fin de réunion
            if(startDateDesired.before(startDateMeeting) && endDateDesired.after(endDateMeeting)) {
                roomToDelete = ((Meeting) meeting).getRoom();
//                Log.d(LOG, "[LOG CONFLICT] La réunion souhaiter se situe autour d'une autre réunion !");
            }

            // Si la date de début se situe pendant la réunion
            if(startDateDesired.after(startDateMeeting) && startDateDesired.before(endDateMeeting)) {
                roomToDelete = ((Meeting) meeting).getRoom();
//                Log.d(LOG, "[LOG CONFLICT] L'heure de début souhaiter se situe pendant une autre réunion !");
            }

            // Si la date de fin se situe pendant la réunion
            if(endDateDesired.after(startDateMeeting) && endDateDesired.before(endDateMeeting)) {
                roomToDelete = ((Meeting) meeting).getRoom();
//                Log.d(LOG, "[LOG CONFLICT] L'heure de fin souhaiter se situe pendant une autre réunion !");
            }

            // Suppression de la réunion
            if(roomToDelete != null) {
                meetingRooms.remove(((Meeting) meeting).getRoom());
            }

//            Log.d(LOG, "[LOG] ---------------------------------------------------");

        }

//        Log.d(LOG, "[LOG] ROOMS AVAILABLE: " + meetingRooms.size());
        return meetingRooms;
    }

    @Override
    public List<Object> getMeetingsFilterByDate(Calendar date) {

        List<Object> datas = new ArrayList<>();

        for(Object meeting : meetings) {

            String meetingDate = dateFR.format(((Meeting) meeting).getStartDate().getTime());
            String expectedDate = dateFR.format(date.getTime());

            if(meetingDate.equals(expectedDate)) {
                datas.add(meeting);
            }
        }
        return datas;
    }

    @Override
    public List<Object> getMeetingsFilterByPlace(int name) {

        List<Object> datas = new ArrayList<>();

        for(Object meeting : meetings) {
            if(((Meeting) meeting).getRoom().getName() == name) {
                datas.add(meeting);
            }
        }

        return datas;
    }


    /**
     * Returns all meetings
     * @return
     */
    @Override
    public List<Object> getMeetings() {
        return meetings;
    }

    /**
     * Add new meeting
     */
    @Override
    public void addMeeting(Meeting meeting) {
        meetings.add(meeting);
    }

    /**
     * Delete meeting
     */
    @Override
    public void deleteMeeting(Meeting meeting) {
        meetings.remove(meeting);
    }

    /**
     * Add new participants to meeting
     * @param meeting
     * @param emailParticipants
     */
    @Override
    public void addParticipant(Meeting meeting, String[] emailParticipants) {
        for(String emailParticipant : emailParticipants) {
            meeting.getParticipants().add(new Participant(emailParticipant));
        }
    }

}
