package com.valentin.mareu.di;

import android.content.Context;
import android.util.Log;

import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.service.DummyMeetingApiService;
import com.valentin.mareu.service.MeetingApiService;

/**
 * Dependency injector to get instance of services
 */
public class DI {

    private static MeetingApiService service = null;

    /**
     * Get an instance on @{@link MeetingApiService}
     * @return
     */
    public static MeetingApiService getMeetingApiService() {
        if(service == null) {
            service = (MeetingApiService) new DummyMeetingApiService();
        }
        return service;
    }

    /**
     * Get always a new instance on @{@link MeetingApiService}. Useful for tests, so we ensure the context is clean.
     * @return
     */
    public static MeetingApiService getNewInstanceApiService() {
        if(service == null) {
            service = (MeetingApiService) new DummyMeetingApiService();
        }
        return service;
    }
}

