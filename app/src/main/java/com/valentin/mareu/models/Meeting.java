package com.valentin.mareu.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class Meeting implements Parcelable {

    private int id;
    private String subject;
    private Calendar startDate;
    private Calendar endDate;
    private Room room;
    private List<Participant> participants;

    /**
     * Constructor
     * @param id
     * @param subject
     * @param startDate
     * @param endDate
     * @param room
     * @param participants
     */
    public Meeting(int id, String subject, Calendar startDate, Calendar endDate, Room room, List<Participant> participants) {
        this.id = id;
        this.subject = subject;
        this.startDate = startDate;
        this.endDate = endDate;
        this.room = room;
        this.participants = participants;
    }

    protected Meeting(Parcel in) {
        id = in.readInt();
        subject = in.readString();

        long milliseconds_startDate = in.readLong();
        String timezone_id_startDate = in.readString();
        startDate = new GregorianCalendar(TimeZone.getTimeZone(timezone_id_startDate));
        startDate.setTimeInMillis(milliseconds_startDate);

        long milliseconds_endDate = in.readLong();
        String timezone_id_endDate = in.readString();
        endDate = new GregorianCalendar(TimeZone.getTimeZone(timezone_id_endDate));
        endDate.setTimeInMillis(milliseconds_endDate);

        room = (Room) in.readParcelable(Room.class.getClassLoader());

        if (in.readByte() == 0x01) {
            participants = new ArrayList<Participant>();
            in.readList(participants, Participant.class.getClassLoader());
        } else {
            participants = null;
        }
    }

    public static final Creator<Meeting> CREATOR = new Creator<Meeting>() {
        @Override
        public Meeting createFromParcel(Parcel in) {
            return new Meeting(in);
        }

        @Override
        public Meeting[] newArray(int size) {
            return new Meeting[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(subject);

        parcel.writeLong(startDate.getTimeInMillis());
        parcel.writeString(startDate.getTimeZone().getID());

        parcel.writeLong(endDate.getTimeInMillis());
        parcel.writeString(endDate.getTimeZone().getID());

        parcel.writeParcelable(room, i);

        if (participants == null) {
            parcel.writeByte((byte) (0x00));
        } else {
            parcel.writeByte((byte) (0x01));
            parcel.writeList(participants);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }
}
