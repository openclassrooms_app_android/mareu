package com.valentin.mareu.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

import androidx.annotation.ColorRes;
import androidx.annotation.StringRes;

public class Room implements Parcelable {

    private int id;
    @StringRes private int name;
    @ColorRes private int color;

    public Room(int id, int name, int color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    protected Room(Parcel in) {
        id = in.readInt();
        name = in.readInt();
        color = in.readInt();
    }

    public static final Creator<Room> CREATOR = new Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel in) {
            return new Room(in);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(name);
        parcel.writeInt(color);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return id == room.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
