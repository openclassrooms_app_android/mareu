package com.valentin.mareu.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Participant implements Parcelable {

    private String email;

    /**
     * Constructor
     * @param email
     */
    public Participant(String email) {
        this.email = email;
    }


    protected Participant(Parcel in) {
        email = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Participant> CREATOR = new Creator<Participant>() {
        @Override
        public Participant createFromParcel(Parcel in) {
            return new Participant(in);
        }

        @Override
        public Participant[] newArray(int size) {
            return new Participant[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
