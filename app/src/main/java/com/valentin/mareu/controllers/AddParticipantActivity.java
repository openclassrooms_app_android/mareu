package com.valentin.mareu.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.valentin.mareu.R;
import com.valentin.mareu.adapters.AddParticipantAdapter;
import com.valentin.mareu.di.DI;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.service.MeetingApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddParticipantActivity extends AppCompatActivity {

    private static final String LOG = AddParticipantActivity.class.getSimpleName();
    private Context context;

    // Intent
    private Meeting meeting;

    // ELEMENTS
    private LinearLayout mLayout;
    private TextView mTitle;
    private Button mValidateButton;
    private TextView errorText;

    private int nbParticipants;
    private EditText participantInput;
    private List<EditText> allEds;
    private List<Boolean> allEdsBoolean;
    private RecyclerView mRecyclerView;
    private AddParticipantAdapter addParticipantAdapter;


    // VALIDATION
    private String[] emails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_participant);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Ajout des participants");

        Log.d(LOG, "[LOG PAGE] --> Page ajout des participants !");

        context = AddParticipantActivity.this;

        // INIT SERVICE
        MeetingApiService apiService = DI.getMeetingApiService();

        /**
         * PREVIOUS BUTTON
         */
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        /**
         * GET ELEMENTS
         */
        mLayout = (LinearLayout) findViewById(R.id.activity_add_participant_layout);
        mTitle = (TextView) findViewById(R.id.activity_add_participant_title);
        mValidateButton = (Button) findViewById(R.id.activity_add_participant_validate_button);
        mValidateButton.setEnabled(false);
        errorText = (TextView) findViewById(R.id.activity_add_participant_errors);

        // Intent
        meeting = getIntent().getParcelableExtra("MEETING");

        // DIALOG
        int MIN_PARTICIPANTS = 2;
        int MAX_PARTICIPANTS = 5;
        String[] nums = new String[MAX_PARTICIPANTS - MIN_PARTICIPANTS + 1];
        for(int i = 2; i <= MAX_PARTICIPANTS; i++) {
            nums[i - 2] = Integer.toString(i);
        }

        NumberPicker nbParticipantPicker = new NumberPicker(this);
        nbParticipantPicker.setDisplayedValues(nums);
        nbParticipantPicker.setMinValue(MIN_PARTICIPANTS);
        nbParticipantPicker.setMaxValue(MAX_PARTICIPANTS);
        nbParticipantPicker.setWrapSelectorWheel(true);
        nbParticipantPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        nbParticipants = MIN_PARTICIPANTS;
        nbParticipantPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldValue, int newValue) {
                nbParticipants = newValue;
            }
        });

        // Create Dialog
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Combien de participant souhaitez-vous ajouter ?")
                .setCancelable(false)
                .setPositiveButton("Ok", null)
                .setNegativeButton("Annuler", null)
                .setView(nbParticipantPicker)
                .show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_add_participant_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(nbParticipants != 0) {

                    allEds = new ArrayList<EditText>();

                    addParticipantAdapter = new AddParticipantAdapter(nbParticipants, mValidateButton, allEds, errorText);
                    mRecyclerView.setAdapter(addParticipantAdapter);

                    mTitle.setVisibility(View.VISIBLE);
                    mValidateButton.setVisibility(View.VISIBLE);

                    dialog.dismiss();
                }
            }
        });

        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                finish();
            }
        });

        // VALIDATION MEETING AND PARTICIPANTS
        mValidateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // GET PARTICIPANT EMAILS
                emails = new String[addParticipantAdapter.getItemCount()];
                for(int i = 0; i < addParticipantAdapter.getItemCount(); i++) {
                    emails[i] = allEds.get(i).getText().toString();
                }

                // Create resource
                apiService.addMeeting(meeting);
                apiService.addParticipant(meeting, emails);

                Toast.makeText(getApplicationContext(), context.getResources().getString(R.string.success_add_meeting), Toast.LENGTH_LONG).show();

                Intent mainActivity = new Intent(AddParticipantActivity.this, MainActivity.class);
                startActivity(mainActivity);

            }
        });

    } // onCreate

} // class