package com.valentin.mareu.controllers;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.valentin.mareu.R;
import com.valentin.mareu.di.DI;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Participant;
import com.valentin.mareu.models.Room;
import com.valentin.mareu.service.MeetingApiService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class AddMeetingActivity extends AppCompatActivity {

    private static final String LOG = AddMeetingActivity.class.getSimpleName();

    private Context context;
    MeetingApiService apiService;
    private List<Room> meetingRooms;
    private List<Room> listOfRooms;

    // ELEMENTS
    private Button dateButton;
    private Button startHourButton;
    private Button endHourButton;
    private EditText subject;
    private TextView errorText;
    private Button addParticipantButton;
    private Spinner spinnerMeetingRooms;

    // TimePicker
    private int lastSelectedStartHour;
    private int lastSelectedStartMinute;
    private int lastSelectedEndHour;
    private int lastSelectedEndMinute;
    private Calendar startDate;
    private Calendar endDate;
    private Calendar selectedDate;

    // WATCHERS
    private Boolean subjectIsNotEmpty = false;
    private Boolean dateIsNotEmpty = false;
    private Boolean startHourIsNotEmpty = false;
    private Boolean endHourIsNotEmpty = false;
    private Boolean spinnerMeetingRoomsIsNotEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meeting);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Ajout d'une réunion");

        Log.d(LOG, "[LOG PAGE] --> Page Ajout de réunion");

        // Current Date
        Calendar currentDate = Calendar.getInstance(Locale.FRANCE);
        currentDate.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        currentDate.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
        currentDate.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        context = AddMeetingActivity.this;

        // INIT SERVICE
        apiService = DI.getMeetingApiService();
        listOfRooms = apiService.getAllMeetingRooms();

        // DATE FORMATTER
        SimpleDateFormat formatFR = new SimpleDateFormat("dd/MM/y HH:mm", Locale.FRANCE);
        SimpleDateFormat dateFR = new SimpleDateFormat("dd/MM/y", Locale.FRANCE);
        SimpleDateFormat hourFR = new SimpleDateFormat("HH:mm", Locale.FRANCE);

        /**
         * GET ELEMENTS
         */
        dateButton = (Button) findViewById(R.id.activity_add_meeting_dateButton);
        startHourButton = (Button) findViewById(R.id.activity_add_meeting_startHourButton);
        endHourButton = (Button) findViewById(R.id.activity_add_meeting_endHourButton);
        subject = (EditText) findViewById(R.id.activity_add_meeting_subject);
        errorText = (TextView) findViewById(R.id.activity_add_meeting_errors);
        addParticipantButton = (Button) findViewById(R.id.activity_add_meeting_addParticipantButton);
        spinnerMeetingRooms = (Spinner) findViewById(R.id.activity_add_meeting_meetingRooms);

        spinnerMeetingRooms.setEnabled(false);
        addParticipantButton.setEnabled(false);

        startDate = Calendar.getInstance(Locale.FRANCE);
        endDate = Calendar.getInstance(Locale.FRANCE);
        selectedDate = Calendar.getInstance(Locale.FRANCE);

        /**
         * PREVIOUS BUTTON
         */
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // Subject Listener
        subject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                subjectIsNotEmpty = editable.length() > 0;
                displayContinueButton();
            }
        });

        /**
         * Date Picker
         */

         //dateDialog Listener
        DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {

                startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDate.set(Calendar.MONTH, month);
                startDate.set(Calendar.YEAR, year);
                endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDate.set(Calendar.MONTH, month);
                endDate.set(Calendar.YEAR, year);

                String dateText = dateFR.format(startDate.getTime());
                dateButton.setText(dateText);

                dateIsNotEmpty = true;
                displaySpinnerMeetingRooms();
                displayContinueButton();
            }
        };

        DatePickerDialog dateDialog = new DatePickerDialog(this, dateListener, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        dateDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialog.show();
            }
        });

        /**
         * Hours TimePicker
         */

        // startHourDialog Listener
        TimePickerDialog.OnTimeSetListener startHourListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                startDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                startDate.set(Calendar.MINUTE, minute);

                String textStartHourButton = hourFR.format(startDate.getTime());
                startHourButton.setText(textStartHourButton);

                startHourIsNotEmpty = true;
                displaySpinnerMeetingRooms();
                displayContinueButton();

            }
        };

        // Create startHourDialog:
        TimePickerDialog startHourDialog = new TimePickerDialog(this, startHourListener, lastSelectedStartHour, lastSelectedStartMinute, true);

        // Show startHourDialog
        startHourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startHourDialog.show();
            }
        });

        // endHourDialog Listener
        TimePickerDialog.OnTimeSetListener endHourListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                endDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                endDate.set(Calendar.MINUTE, minute);

                String textEndHourButton = hourFR.format(endDate.getTime());
                endHourButton.setText(textEndHourButton);

                endHourIsNotEmpty = true;
                displaySpinnerMeetingRooms();
                displayContinueButton();
            }
        };

        // Create endHourDialog:
        TimePickerDialog endHourDialog = new TimePickerDialog(this, endHourListener, lastSelectedStartHour, lastSelectedStartMinute, true);

        // Show startHourDialog
        endHourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endHourDialog.show();
            }
        });

        /**
         * Meeting rooms Spinner
         */

        /**
         * Go to next activity addParticipant
         */
        addParticipantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Room roomSelected = null;
                int id = apiService.getMeetings().size() != 0 ? apiService.getMeetings().size() + 1 : 0;
                int idRoom = listOfRooms.size();
                String expectedNameRoom = spinnerMeetingRooms.getSelectedItem().toString().toLowerCase();

                for(Room room : listOfRooms) {
                    String nameRoom = (context.getResources().getString(R.string.prefix_meeting_rooms_fr) + " " + context.getResources().getString(room.getName())).toLowerCase();
                    if(nameRoom.equals(expectedNameRoom)) {
                        roomSelected = new Room(idRoom, room.getName(), room.getColor());
                    }
                }

                List<Participant> participants = new ArrayList<>();
                Meeting meeting = new Meeting(id, subject.getText().toString(), startDate, endDate, roomSelected, participants);

                Intent participantPage = new Intent(AddMeetingActivity.this, AddParticipantActivity.class);
                participantPage.putExtra("MEETING", meeting);
                startActivity(participantPage);
            }
        });

    } // onCreate

    // Display Add Participant Button
    public void displayContinueButton() {
        Boolean hoursIsCorrect = false;

        // Check hours
        if(startHourIsNotEmpty && endHourIsNotEmpty) {

            if(startDate.before(endDate) && !startDate.equals(endDate)) {
                hoursIsCorrect = true;
            }

            // IF STARTDATE IS AFTER ENDDATE
            if(startDate.after(endDate)) {
                errorText.setText(R.string.error_startdate_is_after_enddate);
            }

            // IF DATE IS EQUALS
            if(startDate.equals(endDate)) {
                errorText.setText(R.string.error_startdate_equals_enddate);
            }

            // IF STARTDATE IS NOT AFTER AND NOT EQUALS ENDDATE
            if(!startDate.after(endDate) && !startDate.equals(endDate)) {
                errorText.setVisibility(View.GONE);
            } else {
                errorText.setVisibility(View.VISIBLE);
            }
        }

        addParticipantButton.setEnabled(hoursIsCorrect &&
                subjectIsNotEmpty &&
                dateIsNotEmpty &&
                startHourIsNotEmpty &&
                endHourIsNotEmpty &&
                spinnerMeetingRoomsIsNotEmpty);
    }

    public void displaySpinnerMeetingRooms() {

        if(startDate.before(endDate) && dateIsNotEmpty && startHourIsNotEmpty && endHourIsNotEmpty) {
            spinnerMeetingRooms.setEnabled(true);
            spinnerMeetingRooms.setVisibility(View.VISIBLE);

            List<Room> getMeetingRoomsAvailable = apiService.getMeetingRoomsAvailable(startDate, endDate);
            ArrayList<String> meetingRooms = new ArrayList<>();
            meetingRooms.add(0, context.getResources().getString(R.string.placeholder_meeting_rooms));
            for(Room room : getMeetingRoomsAvailable) {
                meetingRooms.add(context.getResources().getString(R.string.prefix_meeting_rooms_fr) + " " + context.getResources().getString(room.getName()));
            }

            ArrayAdapter<String> adapterSpinnerMeetingRooms = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, meetingRooms) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };

            adapterSpinnerMeetingRooms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerMeetingRooms.setAdapter(adapterSpinnerMeetingRooms);

            // spinnerMeetingRooms Listener
            spinnerMeetingRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);
                    // If user change the default selection
                    // First item is disable and it is used for hint
                    if(position > 0) {
                        Toast.makeText
                                (getApplicationContext(), "Vous avez choisi la '" + selectedItemText + "' !", Toast.LENGTH_SHORT)
                                .show();

                        spinnerMeetingRoomsIsNotEmpty = true;
                        displayContinueButton();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            spinnerMeetingRooms.setEnabled(false);
            spinnerMeetingRooms.setVisibility(View.GONE);
        }
    }

}