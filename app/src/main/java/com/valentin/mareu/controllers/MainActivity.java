package com.valentin.mareu.controllers;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.valentin.mareu.R;
import com.valentin.mareu.adapters.MeetingAdapter;
import com.valentin.mareu.di.DI;
import com.valentin.mareu.enums.FilterName;
import com.valentin.mareu.listeners.MeetingListener;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Room;
import com.valentin.mareu.service.MeetingApiService;
import com.valentin.mareu.service.MeetingGenerator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements MeetingListener {

    private static final String LOG = MainActivity.class.getSimpleName();
    private Context context;
    private MeetingApiService mApiService;
    private List<Room> listOfRooms;

    private SimpleDateFormat formatFR;
    private SimpleDateFormat dateFR;
    private SimpleDateFormat hourFR;

    private LinearLayout mEmptyView;
    private TextView mEmptyViewMessage;
    private String emptyViewMessage;
    private RecyclerView mRecyclerView;
    private MeetingAdapter meetingAdapter;
    private MeetingAdapter getAdapter;
    private List<Object> meetings;
    private ArrayList<String> allMeetingRooms;

    private String meetingStartDate = "";

    // DIALOG
    private DatePickerDialog datePickerDialog;
    private RadioGroup filterButtonGroup;
    private RadioButton dateButtonFilter;
    private Calendar dateSelectedFilter;
    private Boolean dateIsNotEmpty = false;

    private RadioButton placeButtonFilter;
    private Spinner spinnerMeetingRooms;
    private Boolean meetingRoomIsNotEmpty = false;

    private Button negativeButton;
    private Button positiveButton;
    private FloatingActionButton resetFilterButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        mApiService = DI.getMeetingApiService();
        Log.d(LOG, "[LOG] --> Démarrage du listing (Meetings) ! + meetingAdapter = " + meetingAdapter);

        listOfRooms = mApiService.getAllMeetingRooms();

        // DATE FORMATTER
        formatFR = new SimpleDateFormat("dd/MM/y HH:mm", Locale.FRANCE);
        dateFR = new SimpleDateFormat("dd/MM/y", Locale.FRANCE);
        hourFR = new SimpleDateFormat("HH:mm", Locale.FRANCE);

        // ICON MODIFIED
        Toolbar toolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);

        mEmptyView = (LinearLayout) findViewById(R.id.activity_main_empty_view);
        mEmptyViewMessage = (TextView) findViewById(R.id.activity_main_empty_message);
        emptyViewMessage = context.getResources().getString(R.string.prefix_empty_view) + " !";

        /**
         * RECYCLER VIEW
         */
        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerView);
        meetings = mApiService.getMeetings();
        meetingAdapter = new MeetingAdapter(this, meetings, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setAdapter(meetingAdapter);

        getAdapter = (MeetingAdapter) mRecyclerView.getAdapter();

        // Reset Filter Button Listener
        resetFilterButton = findViewById(R.id.activity_main_reset_filter);
        resetFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                meetings = mApiService.getMeetings();
                getAdapter.updateList(meetings);
                resetFilterButton.setVisibility(View.GONE);
                emptyViewMessage = context.getResources().getString(R.string.prefix_empty_view) + " !";
                Toast.makeText(getApplicationContext(), context.getResources().getString(R.string.success_reset_filters), Toast.LENGTH_LONG).show();
            }
        });

        // Action on click to fab button
        FloatingActionButton fab = findViewById(R.id.activity_main_add_meeting);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addMeeting = new Intent(MainActivity.this, AddMeetingActivity.class);
                startActivity(addMeeting);
            }
        });
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String expectedItemTitle = getResources().getString(R.string.menu_main_filter).toLowerCase();

        if(item.getTitle().toString().toLowerCase().equals(expectedItemTitle)) {

            AlertDialog filterDialog = new AlertDialog.Builder(this)
                    .setTitle("Filtrer par :")
                    .setPositiveButton("Ok", null)
                    .setNegativeButton("Annuler", null)
                    .setView(R.layout.activity_main_filters_dialog)
                    .show();

            // GET DIALOG ELEMENTS
            negativeButton = filterDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            positiveButton = filterDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            dateButtonFilter = (RadioButton) filterDialog.findViewById(R.id.activity_main_filters_dialog_date);
            placeButtonFilter = (RadioButton) filterDialog.findViewById(R.id.activity_main_filters_dialog_place);
            spinnerMeetingRooms = (Spinner) filterDialog.findViewById(R.id.activity_main_filters_dialog_spinnerMeetingRooms);

            positiveButton.setEnabled(false);
            spinnerMeetingRooms.setEnabled(false);

            // datePickerDialog LISTENER
            dateSelectedFilter = Calendar.getInstance(Locale.FRANCE);
            DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                    // Selected date
                    dateSelectedFilter.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    dateSelectedFilter.set(Calendar.MONTH, month);
                    dateSelectedFilter.set(Calendar.YEAR, year);

                    // Set text
                    String text = "Date selectionné: " + dateFR.format(dateSelectedFilter.getTime());
                    dateButtonFilter.setText(text);

                    // Define empty message
                    emptyViewMessage = context.getResources().getString(R.string.prefix_empty_view) + " le '" + dateFR.format(dateSelectedFilter.getTime()) + "' !";

                    dateIsNotEmpty = true;
                    displayPositiveButton();
                }
            };

            // datePickerDialog NEW INSTANCE
            datePickerDialog = new DatePickerDialog(this, datePickerListener, dateSelectedFilter.get(Calendar.YEAR), dateSelectedFilter.get(Calendar.MONTH), dateSelectedFilter.get(Calendar.DAY_OF_MONTH));


            // POSITIVE BUTTON LISTENER
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(dateButtonFilter.isChecked()) {
                        meetings = formatterDataWithHeading(FilterName.DATE, mApiService.getMeetingsFilterByDate(dateSelectedFilter));
                    }

                    @StringRes int name = 0;
                    if(placeButtonFilter.isChecked()){
                        for(Room room : listOfRooms) {
                            String expectedName = context.getResources().getString(R.string.prefix_meeting_rooms_fr).toLowerCase() + " " + context.getResources().getString(room.getName()).toLowerCase();
                            if(expectedName.equals(spinnerMeetingRooms.getSelectedItem().toString().toLowerCase())){
                                name = room.getName();
                            }
                        }
                        meetings = formatterDataWithHeading(FilterName.PLACE, mApiService.getMeetingsFilterByPlace(name));
                    }

                    if(getAdapter != null){
                        getAdapter.updateList(meetings);
                    }

                    resetFilterButton.setVisibility(View.VISIBLE);
                    filterDialog.hide();

                }
            });

            /**
             * DATE RADIO BUTTON LISTENER
             */
            dateButtonFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    meetingRoomIsNotEmpty = false;
                    displayPositiveButton();

                    placeButtonFilter.setChecked(false);
                    spinnerMeetingRooms.setEnabled(false);
                    datePickerDialog.show();
                }
            });

            /**
             * PLACE RADIO BUTTON LISTENER
             */
            placeButtonFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dateIsNotEmpty = false;
                    displayPositiveButton();

                    dateButtonFilter.setChecked(false);
                    dateButtonFilter.setText(getResources().getString(R.string.activity_main_filters_dialog_date));
                    spinnerMeetingRooms.setEnabled(true);

                    // Create list of meeting rooms for filter place
                    allMeetingRooms = new ArrayList<>();
                    allMeetingRooms.add(0, context.getResources().getString(R.string.placeholder_meeting_rooms));
                    for(Room room : listOfRooms) {
                        allMeetingRooms.add(context.getResources().getString(R.string.prefix_meeting_rooms_fr) + " " + context.getResources().getString(room.getName()));
                    }

                    ArrayAdapter<String> adapterSpinnerMeetingRooms = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, allMeetingRooms) {
                        @Override
                        public boolean isEnabled(int position) {
                            return position != 0;
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if (position == 0) {
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            } else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;
                        }
                    };

                    adapterSpinnerMeetingRooms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerMeetingRooms.setAdapter(adapterSpinnerMeetingRooms);

                    // spinnerMeetingRooms Listener
                    spinnerMeetingRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String selectedItemText = (String) parent.getItemAtPosition(position);

                            if (position > 0) {
                                Toast.makeText(getApplicationContext(), "Vous avez choisi la " + selectedItemText, Toast.LENGTH_SHORT).show();
                                emptyViewMessage = context.getResources().getString(R.string.prefix_empty_view) + " en '" + selectedItemText + "' !";

                                meetingRoomIsNotEmpty = true;
                                displayPositiveButton();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    public void displayPositiveButton() {
        positiveButton.setEnabled(dateIsNotEmpty || meetingRoomIsNotEmpty);
    }

    public List<Object> formatterDataWithHeading(FilterName filterBy, List<Object> meetings) {

        List<String> distinctDate = new ArrayList<String>();
        List<String> distinctRoom = new ArrayList<String>();
        List<Object> data = new ArrayList<Object>();

        for(Object meeting : meetings) {
            // Récupération des dates
            if(filterBy.equals(FilterName.DATE)) {
                meetingStartDate = dateFR.format(((Meeting) meeting).getStartDate().getTime());
                if(!distinctDate.contains(meetingStartDate)) {
                    distinctDate.add(meetingStartDate);
                }
            }

            // Récupération des noms de salle
            if(filterBy.equals(FilterName.PLACE)) {
                String nameRoom = context.getResources().getString(R.string.prefix_meeting_rooms_fr) + " " + context.getResources().getString(((Meeting) meeting).getRoom().getName());
                if(!distinctRoom.contains(nameRoom)) {
                    distinctRoom.add(nameRoom);
                }
            }
        }

        // Stockage des réunions par date de façon ordonnée
        if(filterBy.equals(FilterName.DATE)) {
            for(String date : distinctDate) {
                for(Object meeting : meetings) {
                    meetingStartDate = dateFR.format(((Meeting) meeting).getStartDate().getTime());
                    if(date.equals(meetingStartDate)) {
                        if(!data.contains(meetingStartDate)) {
                            data.add(meetingStartDate);
                        }
                        data.add(meeting);
                    }
                }

            }
        }

        // Stockage des réunions par salle de façon ordonnée
        if(filterBy.equals(FilterName.PLACE)) {
            for(String room : distinctRoom) {
                for(Object meeting : meetings) {
                    String nameRoom = context.getResources().getString(R.string.prefix_meeting_rooms_fr) + " " + context.getResources().getString(((Meeting) meeting).getRoom().getName());
                    if(room.equals(nameRoom)) {
                        if(!data.contains(nameRoom)) {
                            data.add(nameRoom);
                        }
                        data.add(meeting);
                    }
                }
            }
        }

        return data;
    }

    @Override
    public void onDeleteMeeting(Object meeting) {
        mApiService.deleteMeeting((Meeting) meeting);
        Toast.makeText(this, context.getResources().getString(R.string.success_delete_meeting), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayNoDataView(int sizeOfData) {
        if(sizeOfData <= 0) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
            mEmptyViewMessage.setText(emptyViewMessage);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        }
    }
}