package com.valentin.mareu.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.valentin.mareu.R;

import java.util.List;

public class GenericTextWatcher implements TextWatcher {

    private static final String LOG = GenericTextWatcher.class.getSimpleName();

    private View view;
    private Button mValidateButton;
    private List<Boolean> allEdsBoolean;
    private String text;
    private TextView errorText;

    public GenericTextWatcher(View view, Button mValidateButton, List<Boolean> allEdsBoolean, TextView errorText) {
        this.view = view;
        this.mValidateButton = mValidateButton;
        this.allEdsBoolean = allEdsBoolean;
        this.errorText = errorText;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    public void afterTextChanged(Editable editable) {

        text = editable.toString();

        Boolean isNotEmpty = text.length() > 0;
        Boolean isValidEmail = Patterns.EMAIL_ADDRESS.matcher(text).matches();

        // Add true if EditText is not empty and email is valid
        for(int i = 0; i < allEdsBoolean.size(); i++) {
            if(view.getId() == i) {
                allEdsBoolean.set(i, isNotEmpty && isValidEmail);
            }
        }

        // IF EMAIL IS NOT VALID
        if(allEdsBoolean.contains(Boolean.FALSE)) {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(R.string.error_email_not_valid);
        } else {
            errorText.setVisibility(View.GONE);
        }

        mValidateButton.setEnabled(!allEdsBoolean.contains(Boolean.FALSE));
    }
}