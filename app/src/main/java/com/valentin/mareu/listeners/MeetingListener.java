package com.valentin.mareu.listeners;

import com.valentin.mareu.models.Meeting;

public interface MeetingListener {
    void onDeleteMeeting(Object meeting);
    void displayNoDataView(int sizeOfData);
}
