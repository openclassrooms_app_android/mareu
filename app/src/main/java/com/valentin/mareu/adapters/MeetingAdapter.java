package com.valentin.mareu.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.valentin.mareu.R;
import com.valentin.mareu.di.DI;
import com.valentin.mareu.listeners.MeetingListener;
import com.valentin.mareu.models.Meeting;
import com.valentin.mareu.models.Participant;
import com.valentin.mareu.service.MeetingApiService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MeetingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String LOG = MeetingAdapter.class.getSimpleName();

    private Context context;
    private List<Object> datas;
    private MeetingListener mMeetingListener;

    public MeetingAdapter(Context context, List<Object> datas, MeetingListener mMeetingListener) {
        this.context = context;
        this.datas = datas;
        this.mMeetingListener = mMeetingListener;
    }

    public void updateList(List<Object> datas) {
        this.datas = datas;
        notifyDataSetChanged();
        Log.d(LOG, "[LOG ADAPTER] --> Données mis à jour (Size: " + this.datas.size() + ") !");

        mMeetingListener.displayNoDataView(this.datas.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch(viewType) {
            case 0:
                viewHolder = new MeetingDateViewHolder(layoutInflater.inflate(R.layout.activity_main_list_item_date, parent, false));
                break;
            case 1:
                viewHolder = new MeetingInformationViewHolder(layoutInflater.inflate(R.layout.activity_main_list_item, parent, false));
                break;
            default:
                throw new RuntimeException("You have passed an object that can't be analyze !!!");
        }
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MeetingDateViewHolder) {
            String date = (String) datas.get(position);
            ((MeetingDateViewHolder) holder).display(date);
        } else {
            Meeting meeting = (Meeting) datas.get(position);

            List<Participant> participants = meeting.getParticipants();
            ((MeetingInformationViewHolder) holder).display(meeting, participants);

            // On delete meeting
            ((MeetingInformationViewHolder) holder).deleteMeetingButton.setOnClickListener(v ->{
                this.datas.remove(position);
                mMeetingListener.onDeleteMeeting(meeting);

                if(getItemCount() == 1 && datas.get(0).getClass().equals(String.class)) {
                    this.datas.clear();
                }

                mMeetingListener.displayNoDataView(this.datas.size());
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        return !datas.get(position).getClass().equals(Meeting.class) ? 0 : 1;
    }

    // View Holder MeetingDateViewHolder
    static class MeetingDateViewHolder extends RecyclerView.ViewHolder{

        private final TextView mDate;

        public MeetingDateViewHolder(@NonNull View itemView) {
            super(itemView);
            mDate = (TextView) itemView.findViewById(R.id.activity_main_list_item_date);
        }

        void display(String date) {
            mDate.setText(date);
        }

    }

    // View Holder MeetingInformationViewHolder
    static class MeetingInformationViewHolder extends RecyclerView.ViewHolder{

        private final View mCircle;
        private final TextView mInfos;
        private final TextView mParticipants;
        private final ImageButton deleteMeetingButton;

        private GradientDrawable circleDrawable;

        public MeetingInformationViewHolder(@NonNull View itemView) {
            super(itemView);

            mCircle = (View) itemView.findViewById(R.id.activity_main_list_item_circle);
            mInfos = (TextView) itemView.findViewById(R.id.activity_main_list_item_infos);
            mParticipants = (TextView) itemView.findViewById(R.id.activity_main_list_item_participants);
            deleteMeetingButton = (ImageButton) itemView.findViewById(R.id.activity_main_list_item_delete_button);
        }

        void display(Meeting meeting, List<Participant> participants) {

            SimpleDateFormat formatFR = new SimpleDateFormat("dd/MM/y HH:mm", Locale.FRANCE);
            String date = formatFR.format(meeting.getStartDate().getTime());

            // INFOS
            String infosText = itemView.getContext().getResources().getString(R.string.prefix_meeting_rooms_fr) + " " + itemView.getContext().getResources().getString(meeting.getRoom().getName()) + " - " + date + " - " + meeting.getSubject();

            // PARTICIPANTS
            String participantsText = null;
            for (Participant participant : participants) {
                if(participantsText != null) {
                    if(participantsText.substring(participantsText.length() - 1).equals(",")) {
                        participantsText = participantsText + " " + participant.getEmail();
                    } else {
                        participantsText = participantsText + ", " + participant.getEmail();
                    }
                } else {
                    participantsText = participant.getEmail();
                }
            }

            // Set color on circle shape
            circleDrawable = new GradientDrawable();
            circleDrawable.setShape(GradientDrawable.OVAL);
            circleDrawable.setColor(ContextCompat.getColor(itemView.getContext(), meeting.getRoom().getColor()));
            circleDrawable.setStroke(1, ContextCompat.getColor(itemView.getContext(), meeting.getRoom().getColor()));

            circleDrawable.setSize(20, 20);
            mCircle.setBackground(circleDrawable);

            // Set text
            mInfos.setText(infosText);
            mParticipants.setText(participantsText);

        }
    }

}
