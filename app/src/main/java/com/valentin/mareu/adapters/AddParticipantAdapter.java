package com.valentin.mareu.adapters;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.valentin.mareu.R;
import com.valentin.mareu.utils.GenericTextWatcher;

import java.util.ArrayList;
import java.util.List;

public class AddParticipantAdapter extends RecyclerView.Adapter<AddParticipantAdapter.MyViewHolder> {

    private final String LOG = AddParticipantAdapter.class.getSimpleName();

    private final int nbParticipants;
    private final Button mValidateButton;
    private List<EditText> allEds;
    private List<Boolean> allEdsBoolean;
    private TextView errorText;

    public AddParticipantAdapter(int nbParticipants, Button mValidateButton, List<EditText> allEds, TextView errorText) {
        this.nbParticipants = nbParticipants;
        this.mValidateButton = mValidateButton;
        this.allEds = allEds;
        this.errorText = errorText;

        allEdsBoolean = new ArrayList<Boolean>();
    }

    @NonNull
    @Override
    public AddParticipantAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_add_participant_list_item, parent, false);
        return new AddParticipantAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddParticipantAdapter.MyViewHolder holder, int position) {
        holder.display(nbParticipants, position, mValidateButton, allEds, allEdsBoolean, errorText);
    }

    @Override
    public int getItemCount() {
        return nbParticipants;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        private final String LOG = MyViewHolder.class.getSimpleName();

        private final EditText participantInput;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            participantInput = (EditText) itemView.findViewById(R.id.activity_add_participant_list_item_participantInput);
        }

        void display(int nbParticipants, int nbInput, Button mValidateButton, List<EditText> allEds, List<Boolean> allEdsBoolean, TextView errorText) {
            String hintText = "Email du participant #" + (nbInput + 1);
            participantInput.setId(nbInput);
            participantInput.setHint(hintText);
            participantInput.setInputType(InputType.TYPE_CLASS_TEXT);

            allEds.add(nbInput, participantInput);
            allEdsBoolean.add(nbInput, false);

            // Add watcher to input
            GenericTextWatcher textWatcher = new GenericTextWatcher(participantInput, mValidateButton, allEdsBoolean, errorText);
            participantInput.addTextChangedListener(textWatcher);
        }

    }
}
